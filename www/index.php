<?php
header('Content-Type: text/html; charset=utf-8');

$dbName = 'test_docker_compose';
$userName = 'root';
$host = 'db';
$passwd = 'mot2passe2ouf';

$msg_prefixe = "Connexion à la base de donnée ";
try {
    $pdodb = new PDO( "mysql:host=$host;dbname=$dbName", $userName, $passwd );
    }
catch(PDOException $e) {
    echo $msg_prefixe . 'échouée : '.$e->getMessage().'<br';
    }
echo $msg_prefixe . "réussie<br>";

$pdodb->exec('SET NAMES utf8');
$query = "SELECT * FROM ville";
$request = $pdodb->prepare($query);
$request->execute();
$results = $request->fetchAll();
 
echo "<br>Liste des villes: <br>";
echo "<ul>";
foreach ($results as $result) {
	echo "<li>" . $result['nom'].'</li>';
}
echo "</ul>";
?>
